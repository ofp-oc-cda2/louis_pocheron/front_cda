import { defineStore } from 'pinia';
import axios from 'axios';


// the first argument is a unique id of the store across your application
export const useStore = defineStore('main', {

    state: () => {
        
        return {
            users: {},
            userImages: {},
            currentUser: {},
            commentaires: []
        }
    },



    getters: {
        listUsers: (state) => Object.values(state.users),
        
        getUserById(state){
            if(Object.values(state.users).length == 0){
                this.fetchUser();
            }
            return (userId) => state.users[userId]
        },


        getUserImage(state){
            return (userId) => state.users[userId]
        },

        getCommentaire(state){
            return(commentaire) => state.commentaires[commentaire]
        },

        isImageLiked(state){
            return (imageId) => {
                return state.currentUser.likes.some((like) =>{
                    return like.image == imageId
                });
            }
        }, 
        getCurrentUser(state){
            return state.currentUser
        }, 
        
      },


      actions: {

        fetchCommentaires(id){
            axios.get("http://localhost:3500/api/commentaires/image/" + id)
            .then(res => {
              let comms = res.data
              this.commentaires = comms;
              console.log(this.commentaires)
              
            })
            .catch(err =>{
                console.log(err)
            })
        },
        
        addComment(comm){
            this.commentaires.push(comm);
        },
        
        fetchCurrentUser(id){
            console.log("pas fetched");
            axios.get('http://localhost:3500/api/user/data/' + id)
            .then(response => {
                this.currentUser = response.data
                console.log("bien fetché")
            })
            .catch(err =>{
                console.log(err)
            })
        },

        // PEUT ETRE PAS SYNC AVEC LA VUE ?? 
        setImageLiked(imgId, liked){
            if(liked){
                this.currentUser.likes.push(liked)
            }
            else{
                this.currentUser.likes.splice(this.currentUser.likes.map(userLike => userLike.likes).indexOf(imgId), 1)
            }
        },

        fetchUser(){
            axios.get('http://localhost:3500/api/users')
            .then(response => {
                this.users = {}
                this.userImages = {}
                response.data.forEach(user => {
                    this.users[user._id.toString()] = user
                })
            })
            .catch(err => {
                console.log(err)
            })
        },
      }
})

// []