import { createRouter, createWebHistory } from "vue-router";
import home from "@/views/home.vue";
import login from "@/views/login.vue";
import register from "@/views/register.vue";


const routes = [{
    name:"home",
    path: "/",
    component: home
}, {
    name:"login",
    path: "/login",
    component: login 
},{
    name:"register",
    path: "/register",
    component: register 
},{
    name:'profil',
    path:'/profil/:id',
    component:()=>import('@/views/profil.vue')
}
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;

